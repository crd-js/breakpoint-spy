# Breakpoint Spy

```CRD.BreakpointSpy``` watches for breakpoint changes on any HTML. ```CRD.BreakpointSpy``` uses ```window.matchMedia``` browser native function to evaluate a set of defined ```CSS``` queries for breakpoint changes; the matched ```CSS``` query will define the current breakpoint of the page. On ```resize``` ```CRD.BreakpointSpy``` will match all the CSS queries specified and will define a breakpoint, so you can hook up on those breakpoint changes to execute custom scripts (like content display changes, image source changes).

Several ```CRD.BreakpointSpy``` instances can be used at the same time. Each instance can have its own set of rules. Defining rules for the object prototype, will set global rules. All instances will inherit global rules.

If no ```window.matchMedia``` is available (or if its defined and limited - IE < 10), the watcher will unbind from ```resize```.

### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
<script type="text/javascript" src="breakpoint-spy.js"></script>
<script type="text/javascript" src="bootstrap-css-rules.js"></script>
```

Then hook some actions to the breakpoint change:

```javascript
// Initializes Bootstrap Spy
var spy = new CRD.BreakpointSpy();

// Hooks to Breakpoint Spy
jQuery(spy).on('crd.breakpointspy.change', function(event, breakpoint) {
	switch(breakpoint) {
		case 'lg':
			console.log('Desktop large Sizes!');
			break;
	}
});
```
As a functionality provided by ```CRD.Utils``` (thru setting object options, see CRD.Utils documentation), events can be setted as options when initializing the object.

```javascript
// Initializes Bootstrap Spy
var spy = new CRD.BreakpointSpy({
	on : {
		'crd.breakpointspy.change' : function(event, breakpoint) {
        	switch(breakpoint) {
        		case 'lg':
        			console.log('Desktop large Sizes!');
        			break;
        	}
        }
	}
});
```

*A packed and merged source (including function.js, utils.js, breakpoint-spy.js and bootstrap-css-rules.js) is available to use.*

### Arguments

```CRD.AllImagesLoaded``` accepts two arguments:

Argument | Default | Description
-------- | ------- | -----------
options : ```Object``` | ```{ 'rules' : [] }``` | Options to overwrite default values for breakpoint rules.

### Settings

Option | Type | Default | Description
------ | ---- | ------- | -----------
rules | ```Array``` | ```[]``` | Default set of rules. Rules will be inherited from the object prototype (if object prototype has any rule defined).

### Events

Event | Params | Description
----- | ------ | -----------
```crd.breakpointspy.default``` | object : ```CRD.BreakpointSpy``` | Default behavior, triggered when no match media functions are available (IE < 10).
```crd.breakpointspy.always``` | changed : ```Boolean```, breakpoint : ```String```, hdpi : ```Boolean```, object : ```CRD.BreakpointSpy``` | Triggered each time window resizes. First parameter specifies if breakpoint has changed since last execution.
```crd.breakpointspy.change``` | breakpoint : ```String```, hdpi : ```Boolean```, object : ```CRD.BreakpointSpy``` | Breakpoint change, triggered each time a breakpoint changes.
```crd.breakpointspy.{rulename}``` | hdpi : ```Boolean```, object : ```CRD.BreakpointSpy``` | Breakpoint change, triggered each time a specific breakpoint changes (if a rule called ```small``` is defined, ```crd.breakpointspy.small``` event will trigger each time the breakpoint is matched.

### Methods

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | options : ```Object``` | A reference to the current object or class: ```CRD.BreakpointSpy```. | Initializes the instance with the provided options.
setBreakpoint | --- | A reference to the current object or class: ```CRD.BreakpointSpy```. | Matches all defined rules, and sets the current breakpoint.
addRule | name : ```String```, rule : ```String``` | A reference to the current object or class: ```CRD.BreakpointSpy```. | Adds custom rules to the breakpoint spy: rules are CSS based media queries.
addAllThese | rules : ```Array```, hard : ```Boolean``` | A reference to the current object or class: ```CRD.BreakpointSpy```. | Adds rules raw, from a given array of rules. If ```hard``` is set as ```true```, all the inherited or previously defined rules for the instance will be cleared.
clearAllRules | --- | A reference to the current object or class: ```CRD.BreakpointSpy```. | Clear all rules for the current instance. Usefull to reset the inherited rules.
getRule | rule : ```String``` | A ```String``` containing the ```css``` query for the rule. | Get a specific rule by its name.
getDefault | data : ```Array``` | A ```String``` containing the ```css``` query for the default rule. | Gets the default value for a given data set.
getCloser | data : ```Array``` | An ```Object``` containing two properties: breakpoint name (as ```breakpoint```) and source (as ```src```). | Gets the closest breakpoint value for a given data set.
isHDPI | --- | ```Boolean``` | Returns a boolean representing the state of hi dpi screen.
destroy | --- | --- | Unhooks from listened events.

```addRule```, ```addAllThese``` and ```clearAllRules``` can be called as instance function or as static function (thru object prototype). If functions are called as static, the rules will be added or deleted from the object prototype, so all the current and newly initialized instaces will inherit the added or deleted rules.

Note that each rule added to the ```CRD.BreakpointSpy``` instance will trigger its own event. If you add a rule named ```small```, Breakpoint Spy will then trigger a custom event for that rule name: ```crd.breakpointspy.small``` when the specified rule is matched.

In order for the matched rules to be correctly stacked, the definitions of the rules must follow an ascending order - from smaller screens to wider screens -.

### Properties

Property | Type | Description
------ | -------- | -----------
rules | ```Object``` | Array of defined rules (pared as key: name of the rule, and value: ```CSS``` media query).
breakpoint | ```Object``` | Matched breakpoint information, containing the following properties:
breakpoint.current | ```String``` | Higher matched breakpoint.
breakpoint.hdpi | ```Boolean``` | HDPI screen status.
breakpoint.matched | ```Array``` | Stack of matched breakpoints.

### Examples

1) Adds custom rule to match really small devices; the rule will only apply to that instance:

```javascript
// Initializes Bootstrap Spy
var spy = new CRD.BreakpointSpy();

// Adds a new rule for small devices
spy.addRule('xxs', '(max-width: 480px)');

// Hooks to Breakpoint Spy
jQuery(spy).on('crd.breakpointspy.xxs', function() {
	console.log("I'm smaller than 480px!");
});
```

2) Adds custom rules to all the instances:

```javascript
// Adds two custom rules
CRD.BreakpointSpy.prototype.addAllThese([
	['small', '(max-width: 800px)'],
	['large', '(min-width: 801px)']
]);

// Initializes Bootstrap Spy
var spy = new CRD.BreakpointSpy();

// Hooks to Breakpoint Spy
jQuery(spy).on('crd.breakpointspy.small', function() {
	console.log("I'm smaller than 800px!");
});
```

### Bootstrap Rules

Breakpoint Spy comes with a set of predefined CSS rules based on Bootstrap ([http://getbootstrap.com/](http://getbootstrap.com/)).

Those rules are:

- Mobile, max-with: 767px (xs)
- Tablet and small laptops, min-width: 768px (sm)
- Small desktops, min-width: 992px (md)
- Large desktops, min-width: 1200px (lg)

In order to use the predefined rules, [bootstrap-css-rules.js](bootstrap-css-rules.js) must be included.

### Custom Rules

```CRD.BreakpointSpy``` works with its predefined set of CSS rules, or can be configured to work with a custom set of rules.

### Browser Support

Breakpoint Spy works on IE8+ in addition to other modern browsers such as Chrome, Firefox, and Safari.

Be aware that IE < 10 doesn't have full support for media queries.

### Dependencies

- CRD.Utils
- jQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details