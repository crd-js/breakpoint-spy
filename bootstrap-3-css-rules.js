/**
 * Bootstrap CSS rules:
 * - Mobile, max-with: 767px (xs)
 * - Tablet and small laptops, min-width: 768px (sm)
 * - Small desktops, min-width: 992px (md)
 * - Large desktops, min-width: 1200px (lg)
 */

if(typeof CRD !== 'undefined' && CRD.hasOwnProperty('BreakpointSpy')) {
	
	CRD.BreakpointSpy.prototype.addAllThese([
		
		// Default set
		[
			'default',
			'(min-width: 1px)'
		],
		[
			'xs',
			'(max-width: 767px)'
		],
		[
			'sm',
			'(min-width: 768px)'
		],
		[
			'md',
			'(min-width: 992px)'
		],
		[
			'lg',
			'(min-width: 1200px)'
		]
	
	]);
	
}